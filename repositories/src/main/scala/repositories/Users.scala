package repositories
import model._
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

import scala.concurrent.Future

class UsersTable(tag: Tag) extends Table[Users](tag, "users"){
  val id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  val login = column[String]("login", O.Unique)
  val passwd = column[String]("passwd")

  def * = (id.?, login, passwd) <> (model.Users.apply _ tupled, model.Users.unapply)
}

object UsersTable{
  lazy val table = TableQuery[UsersTable]
}

class UsersRepository(db: Database) {
  val table = TableQuery[UsersTable]

  def create(user: Users): Future[Users] =
    db.run(table returning table += user)

  def update(user: Users): Future[Int] =
    db.run(table.filter(_.id === user.id).update(user))

  def delete(user: Users): Future[Int] =
    db.run(table.filter(_.id === user.id).delete)

  def getById(user: Users): Future[Option[Users]] =
    db.run(table.filter(_.id === user.id).result.headOption)

  def userIdByName(userName: String): Future[Boolean] =
    db.run(table.filter(_.login === userName)
      .map(_.id)
      .exists
      .result
    )

  def checkCredentials(login: String, passwd: String) = {
    db.run( table.filter(user => user.login === login && user.passwd === passwd)
        .map(_.id)
        .result
        .headOption)
  }
}
