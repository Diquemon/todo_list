package repositories

import model._
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

import scala.concurrent.Future

class TasksTable(tag: Tag) extends Table[Tasks](tag, "tasks") {
  val id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  val userId = column[Int]("id_user")
  val title = column[String]("title")
  val text = column[String]("text")
  val isActive = column[Boolean]("is_active")

  val userFk = foreignKey("id_user_fk", userId, TableQuery[UsersTable])(_.id)

  def * = (id.?, userId, title, text, isActive) <> (model.Tasks.apply _ tupled, model.Tasks.unapply)

}

object TasksTable {
  lazy val table = TableQuery[TasksTable]
}

class TasksRepository(db: Database) {
  val table = TableQuery[TasksTable]

  def create(task: Tasks): Future[Tasks] =
    db.run(table returning table += task)
  def createTask(userId: Int, title: String, text: String): Future[Tasks] =
    create(new Tasks(userId=userId, title=title, text=text, isActive = true))

  def delete(task: Tasks): Future[Int] =
    db.run(
      table.filter(_.id === task.id)
        .delete
    )
  def deleteById(taskId: Int, userId:Int): Future[Int] =
    db.run(
      table.filter(task => task.id === taskId && task.userId === userId)
        .delete
    )

  def update(task: Tasks): Future[Int] =
    db.run(
      table.filter(_.id === task.id)
        .update(task)
    )

  def getById(taskId: Int): Future[Option[Tasks]] =
    db.run(
      table.filter(_.id === taskId)
        .result.headOption
    )
  def allTasksByUserId(userId: Int): Future[Seq[(Int, String, String)]] = {
    db.run(
      table.filter(task => task.userId === userId)
        .map(task => (task.id, task.title, task.text))
        .sorted
        .result
    )
  }

  def userTasksByIsActive(userId: Int, state: Boolean): Future[Seq[(Int, String, String)]] = {
    db.run(
      table.filter(task => task.userId === userId && task.isActive === state)
        .map(task => (task.id, task.title, task.text))
        .sorted
        .result
    )
  }

  def changeTaskIsActiveById(taskId: Int, userId: Int, state: Boolean) =
    db.run(
      table.filter(task => task.userId === userId && task.id === taskId)
        .map(_.isActive)
        .update(state)
    )

}


