import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import model._
import repositories._

import scala.io.StdIn
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global


object Main {

  val db = Database.forConfig("heroku")
  val tasksRepository = new TasksRepository(db)
  val usersRepository = new UsersRepository(db)

  def init(): Unit = {
    Await.result(db.run(UsersTable.table.schema.create), Duration.Inf)
    Await.result(db.run(TasksTable.table.schema.create), Duration.Inf)
    Await.result(usersRepository.create(Users(login = "data", passwd = "data")), Duration.Inf)
    Await.result(usersRepository.create(Users(login = "root", passwd = "root")), Duration.Inf)
  }

def main(args: Array[String]): Unit = {
  StartMenu.menu()
 while (true) Thread.sleep(Long.MaxValue)
}


  abstract class Menu {
    val actionMap: Map[Int, () => Unit]

    def exit()

    def menu()
  }

  case class UserMenu(uid: Int) extends Menu {
    val actionMap = Map[Int, () => Unit](1 -> activeUserTasks, 2 -> inactiveUserTasks, 3 -> addTask, 4 -> deleteTask, 5 -> deactivateTask, 6->activateTask, 7 -> logout, 8 -> exit)

    def exit(): Unit = sys.exit(0)

    def activeUserTasks(): Unit = {
      println("Active tasks:")
      tasksRepository.userTasksByIsActive(uid, true).onComplete {
        case Success(tasks) =>
          tasks.foreach { case (id, title, text)
          => println(id + ". " + title + "\ntext: " + text)
          }
          println("__________________________")
          menu()
        case Failure(e) =>
          println("Error")
          menu()
      }
    }

    def inactiveUserTasks(): Unit = {
      println("Active tasks:")
      tasksRepository.userTasksByIsActive(uid, false).onComplete {
        case Success(tasks) =>
          tasks.foreach { case (id, title, text)
          => println(id + ". " + title + "\ntext: " + text)
          }
          println("__________________________")
          menu()
        case Failure(e) =>
          println("Error")
          menu()
      }
    }

    def addTask(): Unit = {
      println("Enter title:")
      val title = StdIn.readLine()
      println("Enter text:")
      val text = StdIn.readLine()
      tasksRepository.createTask(uid, title, text)
      menu()
    }

    def deleteTask () = {
      println("Enter id for delete:")
      val id = StdIn.readInt()
      tasksRepository.deleteById(id,uid)
      menu()
    }

    def activateTask() = {
      println("Enter id to activate:")
      val id = StdIn.readInt()
      tasksRepository.changeTaskIsActiveById(id, uid, true)
      menu()
    }

    def deactivateTask() = {
      println("Enter id to deactivate:")
      val id = StdIn.readInt()
      tasksRepository.changeTaskIsActiveById(id, uid, false)
      menu()
    }
    def logout() = {
        StartMenu.menu()
    }
    def menu() = {
      println("1. Active tasks.\n2. Finished tasks.\n3. Add new task.\n4. Delete task.\n5. Deactivate task.\n6. Activate task.\n7. LogOut.\n8. Exit")
      actionMap.get(StdIn.readInt()) match {
        case Some(f) => f()
        case None =>
          println("Something wrong")
          menu()
      }
    }
  }
  case object StartMenu extends Menu {
    val actionMap = Map[Int, () => Unit](1 -> login, 2 -> newUser, 3 -> exit)

    def enterPasswd(): String = {
      println("Password:")
      StdIn.readLine() match {
        case passwd: String if passwd.length > 0 => passwd
        case _ => {
          println("Password is required")
          enterPasswd()
        }
      }
    }

    def enterName(): String = {
      println("Login:")
      StdIn.readLine() match {
        case login: String if login.length > 0 => login
        case _ => {
          println("Login is required")
          enterName()
        }
      }
    }


    def login() = {
      val login = enterName()
      val password = enterPasswd()
      usersRepository.checkCredentials(login, password).onComplete {
        case Success(userIdOption) =>
          userIdOption match {
            case Some(userId) => val umenu = UserMenu(userId)
              umenu.menu()
            case None =>
              println("Something wrong!")
              menu()
          }
        case Failure(_) => println("Something wrong!")
          menu()
      }
    }

    def newUser(): Unit = {
      val login = enterName()
      login match {
        case _ if (Await.result(usersRepository.userIdByName(login), Duration.Inf)) => {
          println("Choose another login")
          newUser()
        }
        case _ =>
          val passwd = enterPasswd()
          usersRepository.create(Users(login = login, passwd = passwd))
      }
      menu()
    }

    def exit() = sys.exit(0)

    def menu(): Unit = {
      println("\n1. Login.\n2. New user.\n3. Exit.")
      actionMap.get(StdIn.readInt()) match {
        case Some(f) => f()
        case None =>
          println("Something wrong")
          menu()
      }
    }
  }

}
