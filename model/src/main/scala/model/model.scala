package object model{
  case class Tasks(id: Option[Int] = None, userId: Int, title: String, text: String, isActive: Boolean)
  case class Users(id: Option[Int] = None, login: String, passwd: String)
}